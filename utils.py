#!/usr/bin/env python3
#
# utils.py
#

from easydict import EasyDict as edict
import numpy as np
import pandas as pd


def get_ylim_from_var(data, force_final_visible=5, std_lim=1, std_pad=.1):
    data = np.asarray(data)
    factor = std_lim + std_pad
    m, s = np.mean(data), np.std(data)
    s_scaled = (std_lim + std_pad) * s
    ylim = (m-s_scaled, m+s_scaled)

    if force_final_visible:
        assert data.ndim == 1
        y = data[-force_final_visible:]
        delta = std_pad * s
        ylim = (min(ylim[0], min(y)-delta), max(ylim[1], max(y)+delta))

    return ylim


def fit(*, x=None, y=None, log=False, return_y_fit=False):
    assert y is not None

    if x is None:
        x = np.arange(len(y))

    A = np.stack((x, np.ones_like(x)), axis=1)
    B = np.log10(y) if log else y
    c1, c0 = np.linalg.lstsq(A, B, rcond=-1)[0]

    if not return_y_fit:
        return c1, c0

    yf = x*c1 + c0
    if log:
        yf = np.exp(yf)

    return c1, c0, yf


def fit_data(ts, variant):
    index = ts.index

    y = ts.values.astype(int)
    x = np.arange(len(y))

    if '_' in variant:
        assert variant.count('_') == 1
        variant, param = variant.split('_')
        param = int(param)

    if variant == 'all':
        xf, yf = x, y
    elif variant == 'last':
        xf, yf = x[-param:], y[-param:]
    elif variant == 'before-last':
        xf, yf = x[-param-1:-1], y[-param-1:-1]
    else:
        raise ValueError()

    c1, c0 = fit(x=xf, y=yf, log=True)

    x_fit = np.arange(-2, len(y)+2)
    #y_fit = 10**(c1*x_fit + c0)
    yl_fit = c1*x_fit + c0
    tsf = pd.Series(10**(c1*x + c0), index=index)

    return edict(x=x_fit, yl=yl_fit, ts=tsf, c0=c0, c1=c1)


def hide_spines_tr(ax, hide_right=True):
    ax.spines['top'].set_visible(False)
    if hide_right:
        ax.spines['right'].set_visible(False)


def fmt_int_for_label(x, unlog=False):
    if unlog:
        x = 10**x

    s = ''

    if x >= 1000:
        x = x / 1000
        s = 'k'

        if x >= 1000:
            x = x / 1000
            s = 'm'

    x_s = round(x)

    return f'{x_s}{s}'
