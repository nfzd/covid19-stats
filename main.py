#!/usr/bin/env python3
#
# main.py
#

from cycler import cycler
from datetime import datetime, timedelta
from easydict import EasyDict as edict
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import numpy as np
import os
from os.path import join
import pandas as pd

from utils import get_ylim_from_var, fit_data, hide_spines_tr, fmt_int_for_label


# parameters

min_date = '2020-09-01'

c_ = [
        # country handle
        # country label: iso 3166-1 alpha-3 codes
        # population numbers: last figure / estimate from wikipedia
        # starting date
        # color
        ('US', 'USA', 328239523, min_date, 'k'),
        ('Germany', 'DEU', 83149300, min_date, 'xkcd:blue'),
        ('Austria', 'AUT', 8902600, min_date, 'xkcd:lightblue'),
        ('Switzerland', 'CHE', 8570146, min_date, 'xkcd:green'),
        ('Italy', 'ITA', 60317116, min_date, 'xkcd:goldenrod'),
        ('United Kingdom', 'GBR', 67886004, min_date, 'xkcd:coral'),
        #('France', 'FRA', 67081000, min_date, 'xkcd:purple'),
        #('Spain', 'ESP', 47431256, min_date, 'xkcd:sienna'),
        ('Sweden', 'SWE', 10367232, min_date, 'xkcd:gray'),
        ('Finland', 'FIN', 5528737, min_date, 'xkcd:purple'),
]

norm = 1e6
norm_s = ' per million'


# add colors and min date

#cmap = cm.viridis
cmap = cm.nipy_spectral

#get_color = lambda k: cmap(np.linspace(0, cmap.N, len(c_), dtype=int)[k])
#get_color = lambda k: cmap(np.linspace(0, cmap.N, len(c_)+1, dtype=int)[k])

#c_ = [(*c, min_date, get_color(k)) for k, c in enumerate(c_)]


# functions

def get_ts(df, country, min_date=None, min_cases=None, log=False, normalize=None, diff=False, running_mean=False):

    if sum(df['Country/Region'] == country) > 1:

        #rows = [(country == df.loc[k]['Country/Region'] and ',' not in df.loc[k]['Province/State']) for k in range(len(df))]
        rows = [country == df.loc[k]['Country/Region'] for k in range(len(df))]

        df = df.iloc[rows].sum()

    else:
        assert sum(df['Country/Region'] == country) == 1
        row = (df['Country/Region'] == country).argmax()

        df = df.iloc[row]

    dates = np.asarray([k for k in df.keys() if k.count('/') == 2])
    index = pd.DatetimeIndex(dates)

    if min_date:
        if running_mean:
            dt = datetime.strptime(min_date, '%Y-%m-%d')
            min_date = (dt - timedelta(days=running_mean)).strftime('%Y-%m-%d')

        m = (index >= min_date)
        index = index[m]
        dates = dates[m]

    ts = pd.Series(df[dates].astype(int), index=index)

    if log:
        ts = ts[ts > 1]

    if normalize:
        ts = pd.Series(ts.values / normalize, index=ts.index)

    if min_cases:
        ts = ts[ts > min_cases]

    if log:
        ts = pd.Series(np.log10(ts.values), index=ts.index)

    if diff:
        ts = pd.Series(np.diff(ts.values), index=ts.index[1:])

    if running_mean:
        assert isinstance(running_mean, int)
        assert running_mean > 1

        N = len(ts.values)
        values = [ts.values[k - running_mean:k].mean() for k in range(running_mean, N)]

        ts = pd.Series(values, index=ts.index[running_mean:])

    return ts


def plot_overview(df, dfl, country, c, population, min_date=None):
    ts = get_ts(df, country, min_date=min_date)
    tsl = get_ts(dfl, country, min_date=min_date)

    # log fit

    fit_def = [
            #('all', 'fit_all'),
            ('last_7', 'fit last 7'),
            ('last_14', 'fit last 14'),
            #('before-last_3', 'fit 3 before last'),
    ]

    fits_v = [fd[0] for fd in fit_def]
    fits_l = [fd[1] for fd in fit_def]

    fits = edict({v: fit_data(ts, v) for v in fits_v})

    c1_k = [*range(-14, 0)]
    c1_x = np.arange(len(c1_k))
    c1_data = [ts[k] / ts[k - 1] for k in c1_k]

    deaths_per_case_k = [*range(-10, 0)]
    deaths_per_case_x = np.arange(len(deaths_per_case_k))
    deaths_per_case = [tsl[k] / ts[k] for k in deaths_per_case_k]

    def lax_formatter(x, pos):
        return fmt_int_for_label(x)

    def num_to_pop_frac(x):
        return x * norm / population

    def text_formatter(x):
        return fmt_int_for_label(num_to_pop_frac(x))

    def rax_formatter(x, pos):
        return text_formatter(x)

    # plot

    cc = cycler(color=['gray']) * cycler(linestyle=['-', '--', ':'])

    h_row1 = 3
    h_row2 = 3
    h_space = 1
    h_row3 = 2
    grid_h_ = [h_row1, h_row2, h_space, h_row3]
    grid_h0_ = [sum(grid_h_[:k]) for k in range(len(grid_h_))]
    grid = (sum(grid_h_), 2)

    fig = plt.figure(figsize=(9, 6))
    plt.subplots_adjust(left=.11, top=.96, right=.92, bottom=.08, hspace=.2, wspace=.4)

    ax = plt.subplot2grid(grid, (grid_h0_[0], 0), rowspan=grid_h_[0], colspan=2)
    ax.set_prop_cycle(cc)
    ax.set_title('{0:s}'.format(country))
    ts.plot(c=c, ls=' ', marker='o', markersize=4, label='')
    [fits[f].ts.plot(lw=2, zorder=-1, label=l) for f, l in zip(fits_v, fits_l)]
    ax.set_xlim(ax.get_xlim()[0] - .5, ax.get_xlim()[1] + .5)
    ax.locator_params(axis='y', nbins=5)
    ax.set_ylabel(r'confirmed cases')
    ax.grid(axis='y')
    leg1 = plt.legend(['{0:s}: {1:d} ({2:d}{3:s})'.format(ts.index[-1].strftime('%d/%m/%Y'), int(ts[-1]), int(num_to_pop_frac(ts[-1])), norm_s)], loc='upper left', handlelength=0, markerscale=0, handletextpad=0)
    ax.legend(loc='lower right')
    ax.add_artist(leg1)
    formatter = mticker.FuncFormatter(lax_formatter)
    ax.yaxis.set_major_formatter(formatter)
    hide_spines_tr(ax)

    rax = ax.twinx()
    rax.set_ylabel(norm_s)
    rax.set_ylim(ax.get_ylim())
    rax.locator_params(axis='y', nbins=5)
    formatter = mticker.FuncFormatter(rax_formatter)
    rax.yaxis.set_major_formatter(formatter)
    hide_spines_tr(rax, hide_right=False)

    plt.setp(ax.get_xticklabels(minor=False), visible=False)
    plt.setp(ax.get_xticklabels(minor=True), visible=False)

    # row 2: deaths

    ax = plt.subplot2grid(grid, (grid_h0_[1], 0), rowspan=grid_h_[1], colspan=2)
    ax.set_prop_cycle(cc)
    tsl.plot(c=c, ls=' ', marker='o', markersize=4, label='')

    ax.set_xlim(ax.get_xlim()[0] - .5, ax.get_xlim()[1] + .5)  # FIXME?
    ax.locator_params(axis='y', nbins=5)
    ax.set_ylabel(r'deaths')
    ax.grid(axis='y')
    leg1 = plt.legend(['{0:s}: {1:d} ({2:d}{3:s})'.format(tsl.index[-1].strftime('%d/%m/%Y'), int(tsl[-1]), int(num_to_pop_frac(tsl[-1])), norm_s)], loc='upper left', handlelength=0, markerscale=0, handletextpad=0)
    formatter = mticker.FuncFormatter(lax_formatter)
    ax.yaxis.set_major_formatter(formatter)
    hide_spines_tr(ax)

    rax = ax.twinx()
    rax.set_ylabel(norm_s)
    rax.set_ylim(ax.get_ylim())
    rax.locator_params(axis='y', nbins=5)
    formatter = mticker.FuncFormatter(rax_formatter)
    rax.yaxis.set_major_formatter(formatter)
    hide_spines_tr(rax, hide_right=False)

    # lower row

    ax = plt.subplot2grid(grid, (grid_h0_[3], 0), rowspan=grid_h_[3])
    ax.plot(np.arange(len(c1_data)), c1_data, c=c, lw=2, marker='o')
    #l3 = ax.axhline(y=np.mean(c1_data[-5:]), c='k', ls=':')
    #l4 = ax.axhline(y=np.mean(c1_data[-10:]), c='gray', ls=':')
    ax.set_ylim(get_ylim_from_var(c1_data, std_pad=.5))
    ax.locator_params(axis='y', nbins=4)
    ax.set_xticks(c1_x)
    ax.set_xticklabels([str(x) if x % 2 == 0 else '' for x in np.asarray(c1_k) + 1])
    ax.set_xlabel(r'day')
    ax.set_ylabel(r'growth factor')
    leg1 = ax.legend(['{0:s}: {1:.3f}'.format(ts.index[-1].strftime('%d/%m/%Y'), c1_data[-1])], loc='upper left', handlelength=0, markerscale=0, handletextpad=0)
    #leg2 = ax.legend([l3, l4], ['mean last 5', 'mean last 10'], loc='lower left')
    #ax.add_artist(leg1)
    hide_spines_tr(ax)

    ax = plt.subplot2grid(grid, (grid_h0_[3], 1), rowspan=grid_h_[3])
    ax.plot(deaths_per_case_x, deaths_per_case, c=c, lw=2, marker='o')
    #ax.set_ylim(get_ylim_from_var(deaths_per_case))
    ax.locator_params(axis='y', nbins=4)
    ax.set_xticks(deaths_per_case_x)
    ax.set_xticklabels(np.asarray(deaths_per_case_k) + 1)
    ax.set_xlabel(r'day')
    ax.set_ylabel(r'deaths per case')
    leg1 = plt.legend(['{0:s}: {1:.3f}'.format(ts.index[-1].strftime('%d/%m/%Y'), deaths_per_case[-1])], loc='upper right', handlelength=0, markerscale=0, handletextpad=0)
    #ax.add_artist(leg1)
    hide_spines_tr(ax)

    [ax.minorticks_off() for ax in fig.axes]

    fn = 'overview_' + country.lower().replace(' ', '_') + '.png'
    plt.savefig(join(outdir, fn))


def plot_new_cases(df, c_, min_date=None, running_mean=7):

    fn = 'new_cases'
    fnc = 'new_cases_comparison'

    # get data

    d_ = []
    dn_ = []

    for country, country_s, pop, _, c in c_:
        ts = get_ts(df, country, min_date=min_date, diff=True)
        tsm = get_ts(df, country, min_date=min_date, diff=True, running_mean=running_mean)
        tsn = get_ts(df, country, min_date=min_date, diff=True, running_mean=running_mean, normalize=pop / norm)
        d_ += [(country, c, ts, tsm)]
        dn_ += [(country_s, c, tsn)]

    # setup plot

    formatter = mticker.FuncFormatter(lambda x, pos: fmt_int_for_label(x))

    # plot

    plt.figure(figsize=(9, 2 * len(d_)))

    for k, (l, c, ts, tsm) in enumerate(d_):
        ax = plt.subplot(len(d_), 1, k + 1)
        if k == 0:
            ax.set_title('new cases')
        ts.plot(c=c, lw=.8)
        #tsm[:-1].plot(c=c, lw=2)  # skip last for mean correction
        tsm.plot(c=c, lw=2)
        ax.set_ylim(0, None)
        ax.locator_params(axis='y', nbins=4)
        ax.grid(axis='y')
        ax.minorticks_off()
        ax.yaxis.set_major_formatter(formatter)
        ax.set_ylabel(l)
        if k == 0:
            ax.legend((ax.lines[-1],), (f'{running_mean}-day mean',), loc='upper left')
        hide_spines_tr(ax)

        # TODO: add relative right axis

    plt.tight_layout()
    plt.savefig(join(outdir, fn + '.png'))

    # comparison of normalized 7-day means

    plt.figure(figsize=(9, 4))

    ax = plt.gca()
    ax.set_title('new cases' + norm_s)
    for k, (l, c, ts) in enumerate(dn_):
        ts.plot(c=c, lw=2, label='{0:s}: {1:s}'.format(l, fmt_int_for_label(int(ts[-1]))))
    ax.locator_params(axis='y', nbins=5)
    ax.grid(axis='y')
    ax.minorticks_off()
    ax.yaxis.set_major_formatter(formatter)
    hide_spines_tr(ax)
    ax.legend(bbox_to_anchor=(1, 0), loc='lower left', fontsize=8)

    plt.tight_layout()
    plt.savefig(join(outdir, fnc + '.png'))


def comparison_log_plot(df, c_, title='confirmed cases', min_date=None, normalize_for_population=False, y_min=None):

    if y_min is not None:
        assert len(y_min) == 2
        y_min = (np.log10(y_min[0]) if y_min[0] is not None else None, np.log10(y_min[1]) if y_min[1] is not None else None)
    else:
        y_min = (None, None)

    fn = 'compare_' + title.replace(' ', '_')

    # get data

    du_ = []
    dn_ = []

    for country, country_s, pop, _, c in c_:
        tsu = get_ts(df, country, min_date=min_date, log=True, normalize=None)
        tsn = get_ts(df, country, min_date=min_date, log=True, normalize=pop / norm)

        lu = fmt_int_for_label(tsu[-1], unlog=True)
        ln = fmt_int_for_label(tsn[-1], unlog=True)
        label = f'{country_s} ({lu} / {ln})'
        du_ += [(label, c, tsu)]
        dn_ += [(label, c, tsn)]

    d_ = [du_, dn_]
    t_ = [title, title + norm_s]

    # setup plot

    scatter_last = False

    def ax_formatter(x, pos):
        val = 10**x
        if val >= 1:
            #val = int(val)
            #s = format(val, 'd')
            s = fmt_int_for_label(val)
        else:
            s = format(val, 'g')
        return s

    formatter = mticker.FuncFormatter(ax_formatter)

    # plot

    fig = plt.figure(figsize=(9, 3))

    for k, (t, d) in enumerate(zip(t_, d_)):
        ax = plt.subplot(1, 2, k + 1)
        ax.set_title(t)
        [ts.plot(c=c, lw=2, label=country) for country, c, ts in d]
        if scatter_last:
            [ts[-1:].plot(marker='o', markersize=8, ls=' ', c=c, label='') for country, c, ts in d]
            ax.set_xlim(0, None)
        ax.set_ylim(y_min[k], None)
        ax.locator_params(axis='y', nbins=4)
        if scatter_last:
            ax.locator_params(axis='x', nbins=4)
            ax.set_xlabel('day')
        if k == 1:
            ax.legend(bbox_to_anchor=(1, 0), loc='lower left', fontsize=8)
        ax.grid(axis='y')
        ax.yaxis.set_major_formatter(formatter)
        hide_spines_tr(ax)

    [ax.minorticks_off() for ax in fig.axes]
    plt.tight_layout()
    plt.savefig(join(outdir, fn + '.png'))


if __name__ == '__main__':

    # pyplot settings

    plt.rcParams.update({'figure.max_open_warning': 0})

    # load data

    datadir = join(os.path.dirname(os.path.abspath(__file__)), 'data')

    df = pd.read_csv(join(datadir, 'time_series_covid19_confirmed_global.csv'))
    dfl = pd.read_csv(join(datadir, 'time_series_covid19_deaths_global.csv'))

    # setup outdir

    outdir = 'plots'
    os.makedirs(join(os.path.dirname(os.path.abspath(__file__)), outdir), exist_ok=True)

    # new cases

    plot_new_cases(df, c_, min_date=min_date)

    # comparison plots

    comparison_log_plot(df, c_, title='confirmed cases', min_date=min_date, y_min=(1000, 100))
    comparison_log_plot(dfl, c_, title='deaths', min_date=min_date, y_min=(100, 10))

    # country details

    [plot_overview(df, dfl, country, c, pop, md) for country, _, pop, md, c in c_]

    # clean up

    plt.close('all')
    #plt.show()
