# covid19-stats

Low quality stats of COVID-19 spreading. Don't take this seriously.

Data source: https://github.com/CSSEGISandData/COVID-19

## overview

* [new cases](#new-cases)
* [comparisons](#comparisons)
* [country details](#country-details)

## new-cases

![compare_confirmed_cases](plots/new_cases_comparison.png)

## comparisons

![compare_confirmed_cases](plots/compare_confirmed_cases.png)
![compare_deaths](plots/compare_deaths.png)

## country details

![overview_us](plots/overview_us.png)
![overview_germany](plots/overview_germany.png)
![overview_austria](plots/overview_austria.png)
![overview_switzerland](plots/overview_switzerland.png)
![overview_united_kingdom](plots/overview_united_kingdom.png)
![overview_italy](plots/overview_italy.png)
![overview_sweden](plots/overview_sweden.png)
![overview_finland](plots/overview_finland.png)

